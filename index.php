<?php 

// include_once("php/constants.php");

// function prueba_crear_contenido() {
//   $data['contenido']['parrafo_1']= '_%S%__%C%_Con el propósito de reconocer el esfuerzo, trabajo y dedicación de 26 alumnos de 6° grado de los sectores educativos N° 10 y 22 de nivel primaria de Ciudad Victoria, asistieron a la Sala de Cabildo del Ayuntamiento a participar en la actividad “Un día en el cabildo”, que por sus grandes logros académicos son dignos representantes de sus escuelas y de su zona escolar.';
//   $data['contenido']['parrafo_2'] = '_%S%_El Encargado del Despacho de la Secretaría de Educación de Tamaulipas, Mario Gómez Monroy  felicitó a nombre del Gobernador Francisco García Cabeza de Vaca a los alumnos destacados, "Por su alto rendimiento académico que han demostrado en sus escuelas a lo largo de este ciclo escolar, por el esfuerzo que han puesto para cumplir con sus deberes y por su desempeño diario en el aula".';
//   $data['contenido']['parrafo_3'] ='_%S%_"Acciones como esta contribuyen a la formación integral de los niños y las niñas tamaulipecos, porque ponen a su alcance los escenarios donde ocurre la política municipal, donde se toman importantes desiciones en beneficio de los victorense y sobre todo que impactan en su vida diaria" añadió Gómez Monroy.';

//   $content = '';

//   foreach ($data as $key => $value) {
//     if($key == 'contenido') {
//       for ($i=1; $i < count($value) + 1; $i++) { 
//         $string = PARAGRAPH.$value['parrafo_'.$i].PARAGRAPH_END;
//         $class = TXTJUS.' ';
//         if(strpos($string, '_%S%_')) {
//           echo 'entro al if';
//           $string = str_replace('_%S%_', '', $string);
//           $class.= TABSPC.' ';
//         }

//         $string = str_replace('_CLASS_', $class, $string);

//         $content.= $string;
//       }
//     }
//   }

//   return $content;
// }

// $contenido = prueba_crear_contenido();

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
  <title>Generar Articulo EDUTAM</title>
</head>
<body>

  <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
    <a class="navbar-brand" href="#">EDUTAM</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav mr-auto">
        <!-- <li class="nav-item active">
          <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#">Link</a>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Dropdown
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdown">
            <a class="dropdown-item" href="#">Action</a>
            <a class="dropdown-item" href="#">Another action</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="#">Something else here</a>
          </div>
        </li>
        <li class="nav-item">
          <a class="nav-link disabled" href="#">Disabled</a>
        </li> -->
      </ul>
    </div>
  </nav>

  <br>

  <div class="container">
    <div class="row">
      <div class="col">
        <h2>Generar Articulo EDUTAM</h2>
        
      </div>
    </div>
  </div>

  <!-- Form para el articulo -->
    <div class="container">
      <div class="row">
        <div class="col">
        <form method="post" action="php/backend.php" enctype="multipart/form-data">
          <div class="form-group">
            <label for="numero_revista">Número de la Revista</label>
            <input type="text" class="form-control" name="numero_revista" id="numero_revista" placeholder="Ejemplo: 1, 2, 3, etc.">
          </div>
          <div class="form-group">
            <label for="numero_revista">Número del Articulo</label>
            <input type="text" class="form-control" name="numero_articulo" id="numero_articulo" placeholder="Ejemplo: 1, 2, 3, etc.">
          </div>
          <div class="form-group">
            <label for="year_revista">Año de la Revista</label>
            <input type="number" class="form-control" name="year_revista" id="year_revista" placeholder="Ejemplo: 2019">
          </div>
          <div class="form-group">
            <label for="select_template">Seleccionar Plantilla</label>
            <select class="form-control" name="plantilla" id="select_template">
              <option value='breves_educativas'>Breves Educativas</option>
            </select>
          </div>
          <div class="form-group">
            <label for="documento-articulo">Subir archivo</label>
            <input type="file" name="documento" class="form-control-file" id="documento-articulo">
          </div>
          <!-- <div class="form-group">
            <label for="exampleFormControlTextarea1">Example textarea</label>
            <textarea class="form-control" id="exampleFormControlTextarea1" rows="20"></textarea>
          </div> -->

          <button type="submit" class="btn btn-primary">Procesar</button>
        </form>
        </div>
      </div>
    </div>

    <div class="container">
      <div class="row">
        <div class="col"><p class="text-justify ">_%S%</p><p class="text-justify tab-space ">El Encargado del Despacho de la Secretaría de Educación de Tamaulipas, Mario Gómez Monroy  felicitó a nombre del Gobernador Francisco García Cabeza de Vaca a los alumnos destacados, &quot;Por su alto rendimiento académico que han demostrado en sus escuelas a lo largo de este ciclo escolar, por el esfuerzo que han puesto para cumplir con sus deberes y por su desempeño diario en el aula&quot;.</p><p class="text-justify tab-space ">&quot;Acciones como esta contribuyen a la formación integral de los niños y las niñas tamaulipecos, porque ponen a su alcance los escenarios donde ocurre la política municipal, donde se toman importantes desiciones en beneficio de los victorense y sobre todo que impactan en su vida diaria&quot; añadió Gómez Monroy.</p>
          <!-- <?=$contenido;?> -->
        </div>
      </div>
    </div>
  <!-- END Form para el artiuclo -->

  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
  
</body>
</html>