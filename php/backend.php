<?php

require 'functions.php';

$documento = $_FILES['documento']['tmp_name'];
// $template = $_POST['plantilla'];
$path = 'archivos/generados/'.$_POST['year_revista'].'/'.$_POST['plantilla'].'/'.$_POST['numero_revista'];

$articulo = read_docx($documento);
$data = build_array(reg_exp($articulo));

$plantilla = select_template($_POST['plantilla']);

check_directory($path);

// $plantilla = str_replace("%titulo%", $data['titulo'], $plantilla);
// $plantilla = str_replace("%lugaryfecha%", $data['lugaryfecha'], $plantilla);

// if (file_put_contents('../'.$path.'/'.$_POST['numero_articulo'].'.php', $plantilla) !== false) {
//   echo "File created (archivo)";
// } else {
//   echo "Cannot create file (archivo)";
// }

// if(!check_file_exists($path, $_POST['numero_articulo'])) {
//   echo 'El archivo que deseas crear ya existe. Revisa tus datos';
//   exit();
// }

$generado = change_content_from_article($data, $plantilla, $path, $_POST['numero_articulo']);
// $generado = create_content($data, $plantilla);

// TODO: crear plantilla para las diferentes tipos de secciones de la Revista

?>

<?php 

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
  <title>Generar Articulo EDUTAM</title>
</head>
<body>

  <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
    <a class="navbar-brand" href="#">EDUTAM</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav mr-auto">
        <!-- <li class="nav-item active">
          <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#">Link</a>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Dropdown
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdown">
            <a class="dropdown-item" href="#">Action</a>
            <a class="dropdown-item" href="#">Another action</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="#">Something else here</a>
          </div>
        </li>
        <li class="nav-item">
          <a class="nav-link disabled" href="#">Disabled</a>
        </li> -->
      </ul>
    </div>
  </nav>

  <br>

  <div class="container">
    <div class="row">
      <div class="col">
        <h2>Generar Articulo EDUTAM</h2>
        
      </div>
    </div>
  </div>

  <!-- Form para el articulo -->
    <div class="container">
      <div class="row">
        <div class="col">
          <div class="alert alert-success">
            Archivo generado correctamente
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col">
          <a class="btn btn-secondary" href="http://localhost/carga_revista">Regresar</a>
        </div>
      </div>
      <div class="row">
        <div class="col">
          <!-- <?=var_dump($data);?> -->
        </div>
      </div>
    </div>
  <!-- END Form para el artiuclo -->

  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
  
</body>
</html>