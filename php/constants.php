<?php
// constantes a utilizar

// TAGS
define('PARAGRAPH', '<p class="_CLASS_">');
define('PARAGRAPH_END', '</p>');

// CLASS
define('TXTJUS', 'text-justify');
define('TXTRGT', 'text-right');
define('TXTLFT', 'text-left');
define('TABSPC', 'tab-space');

?>