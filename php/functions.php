<?php

include_once('constants.php');

function reg_exp($articulo) {
  preg_match_all('/###(.+?)###/', $articulo, $matches);
  return $matches[1];
}

function read_docx($filename){

  $striped_content = '';
  $content = '';

  if(!$filename || !file_exists($filename)) return false;

  $zip = zip_open($filename);
  if (!$zip || is_numeric($zip)) return false;

  while ($zip_entry = zip_read($zip)) {

      if (zip_entry_open($zip, $zip_entry) == FALSE) continue;

      if (zip_entry_name($zip_entry) != "word/document.xml") continue;

      $content .= zip_entry_read($zip_entry, zip_entry_filesize($zip_entry));

      zip_entry_close($zip_entry);
  }
  zip_close($zip);      
  $content = str_replace('</w:r></w:p></w:tc><w:tc>', " ", $content);
  $content = str_replace('</w:r></w:p>', "\r\n", $content);
  $striped_content = strip_tags($content);

  return $striped_content;
}

function build_array($data) {
  // primero tengo que saber que plantilla voy a usar - PENDIENTE
  $cont = 1;
  for ($i=0; $i < count($data); $i++) { 
    $var = explode('__%', $data[$i]);
    // var_dump($var);

    if ($var[0] == 'parrafo') {
      // verificar si existe la simbologia par la sangria
      $array['contenido']['parrafo_'.$cont] = $var[1];
      $cont++;
    } else {
      $array[$var[0]] = $var[1];
    }
  }

  return $array;
}

/**
 * select_template. Retorna la plantilla deseada
 *
 * @param  mixed $template Nombre de la plantilla a utilizar
 *
 * @return void
 */
function select_template($template) {
  return file_get_contents('../archivos/plantillas/'.$template.'_plantilla.php');
}

/**
 * check_directory. Verifica si el directorio existe. En caso de no existir, lo crea
 *
 * @param  mixed $path
 *
 * @return void
 */
function check_directory($path) {
  if(!is_dir('../'.$path)) {
    mkdir('../'.$path, 0777, true);
    chmod('../'.$path, 0777);
  }
}

/**
 * check_if_file_exists. Verifica si el archivo que se intenta crear ya existe en el directorio especificado
 *
 * @param  mixed $path
 * @param  mixed $file
 *
 * @return bool
 */
function check_if_file_exists($path, $file) {
  return (!file_exists('../'.$path.'/'.$file)) ? true : false;
}

/**
 * change_content_from_article. Genera el archivo para el articulo
 *
 * @param  mixed $data  Informacion con la que se llenara el template
 * @param  mixed $template_file Plantilla
 * @param  mixed $path  Directorio donde se almacenara el archivo
 * @param  mixed $num_article Numero del articulo que se creara
 *
 * @return void
 */
function change_content_from_article($data, $template, $path, $num_article) {
  // TODO: Testeando. Crear templates para las secciones de la Revista

  $template = create_content($data, $template);

  if ((file_put_contents('../'.$path.'/'.$num_article.'.php', $template) !== false)) {
    chmod('../'.$path.'/'.$num_article.'.php', 0777); // otorga permisos al archivo generado
    $status = true;
  } else
    $status = false;

  return $status;
}

/**
 * create_content
 *
 * @param  mixed $data  Informacion para generar el contenido del archivo
 * @param  mixed $template Texto de la plantilla
 *
 * @return void
 */
function create_content($data, $template) {
  $content = '';

  foreach ($data as $key => $value) {
    if($key == 'contenido') {
      for ($i=1; $i < count($value) + 1; $i++) { 
        $string = PARAGRAPH.$value['parrafo_'.$i].PARAGRAPH_END;
        $class = TXTJUS.' ';
        if(strpos($string, '_%S%_')) {
          $string = str_replace('_%S%_', '', $string);
          $class.= TABSPC.' ';
        }

        $string = str_replace('_CLASS_', $class, $string);

        $content.= $string;
      }

      $template = str_replace("%".$key."%", $content, $template);
    }

    $template = str_replace("%".$key."%", $value, $template);
  }
  
  // foreach ($data as $key => $value) {
  //   if($key == 'contenido') {
  //     for ($i=1; $i < count($value) + 1; $i++) { 
  //       $content.= $value['parrafo_'.$i];
  //     }
  //     $template = str_replace("%".$key."%", $content, $template);
  //   }
  //   $template = str_replace("%".$key."%", $value, $template);
  // }

  return $template;

}

?>