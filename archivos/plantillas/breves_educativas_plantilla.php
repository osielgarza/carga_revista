<section class="col-12">
	<div class="card-header text-center titulo-secciones">
		<div>
			<h3>Breves Educativas</h3>
		</div>
	</div>

	<div class="card-body revista_secciones"><div class="row">
    <div class="col-12 offset-0  col-sm-12 offset-sm-0  col-md-12 offset-md-0  col-lg-10 offset-lg-1  col-xl-8 offset-xl-2">
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="<?=base_url();?>#Numeros">Revista</a></li>
          <li class="breadcrumb-item"><a href="<?=base_url();?>Revista/Breves_Educativas/%year%/%numero%">Breves Educativas</a></li>
          <li class="breadcrumb-item active" aria-current="page">Artículo <?=$this->uri->segment(5);?></li>
        </ol>
      </nav>
      <hr><br>
    </div>

    <div class="col-12 offset-0  col-sm-12 offset-sm-0  col-md-10 offset-md-1  col-lg-8 offset-lg-2  col-xl-6 offset-xl-3   text-center">
      <h3 class="">%titulo%</h3>
      <br><br>
      <p class="text-left">%lugaryfecha%</p>
      %contenido%
    </div>

    %imagenes%

		<div class="col-12 offset-0  col-sm-12 offset-sm-0  col-md-10 offset-md-1  col-lg-8 offset-lg-2   col-xl-6 offset-xl-3"><hr></div>

		<div class="col-12 offset-0  col-sm-12 offset-sm-0  col-md-10 offset-md-1  col-lg-8 offset-lg-2   col-xl-6 offset-xl-3   text-center">
      <img class="w-100" src="<?=base_url();?>assets/img/one_page/numeros/portadas/Breves_Educativas/%year%/%numero%/%numero_articulo%/1.jpg" alt="_PB_7315">
		</div>

	</div></div>
</section>

